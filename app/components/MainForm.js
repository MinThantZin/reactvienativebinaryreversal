
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class MainForm extends Component {
  render() {
    return (
      <View style={styles.mainform}>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainform: {
   flex:1
  },

});
