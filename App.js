/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';

import mainforn from './app/components/MainForm';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {

  constructor() {
    super()
  
  }


  binaryReversay(input) {
    let inputNumber = parseInt(input, 10)
        .toString(2);
    let length = inputNumber.length;
    let byteStr = `${'0'.repeat(length % 8 === 0 ? 0 :  8 - length % 8)}${inputNumber}`;
    let resultStByte = byteStr.split('')
        .reverse()
        .join('');
   let result= parseInt(resultStByte, 2).toString();
  ToastAndroid.show(result,ToastAndroid.LONG)
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.mainform}>
          <Text style={[styles.title]}
          >
           Binary Reversal
         </Text>

          <TextInput style={[styles.input]}
            placeholder='Enter Number'
            underlineColorAndroid={'red'}
            keyboardType={'numeric'}
            onChangeText={(text) => this.setState({input: text})}
          />



          <TouchableOpacity style={[styles.button]} onPress={() => this.binaryReversay(this.state.input)}
          
              
>
            <Text style={[styles.btnText]}>

              Go !
  </Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 60,
    paddingRight: 60,
    backgroundColor: '#36485f'
  },

  mainform: {
    alignSelf: 'stretch'
  },
  title: {
    fontSize: 24,
    color: 'white',
    marginBottom: 20,
    paddingBottom: 10,

  },
  input: {
    alignSelf: 'stretch',
    height: 40,
    marginBottom: 30,
    color: "white",
    borderBottomColor: 'red'
  },
  button: {
    alignSelf: 'stretch',
    alignItems: 'center',
    padding: 12,
    backgroundColor: '#59cbbd',
    marginTop: 20
  },
  btnText: {

  }

});
